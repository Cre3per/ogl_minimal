/*
 * Credits:
 * LuK1337 - Sigless hook installation
 */

/**
 * Required libraries:
 * - dl
 * - pthread
 */

#include <GL/gl.h>
#include <sys/mman.h> // mprotect

#include <assert.h>
#include <dlfcn.h> // dlopen, dlsym
#include <stdint.h>
#include <thread>
#include <unistd.h> // sysconf





class GLMContext
{

};

typedef void (*fnGLMContext_EndFrame)(GLMContext *);


/**
 * The original GLMContext::EndFrame function
 */
static fnGLMContext_EndFrame s_oGLMContext_EndFrame{ nullptr };


/**
 * Renders a rectangle
 * @param x X
 * @param y Y
 * @param width Width
 * @param height Height
 * @param r Red
 * @param g Green
 * @param b Blue
 * @param a Alpha
 */
void RenderRectangle(GLdouble x, GLdouble y, GLdouble width, GLdouble height, GLdouble r, GLdouble g, GLdouble b, GLdouble a)
{
  glPushMatrix();

  glBegin(GL_TRIANGLE_FAN);

  glColor4d(r, g, b, a);

  glVertex2d(x, y);
  glVertex2d(x + width, y);
  glVertex2d(x + width, y + height);
  glVertex2d(x, y + height);

  glEnd();

  glPopMatrix();
}

/**
 * Sets up the OpenGL environment for 2D rendering
 */
void SetupFrame(void)
{
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  // You'll probably want to change this line, I prefer having a 0.0 - 1.0
  // screen space. You can get CS:GO's screen dimensions using
  // vgui::g_pVGuiSurface->GetScreenSize
  glOrtho(0.0, 1.0, 0.0, 1.0, 0.0, 1.0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // CS:GO is using glFrontFace(GL_CW)
  // Depending on your graphics framework you might want to change it
  // or disable culling as a whole

  // Enable alpha
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

/**
 * Renders a single frame
 */
void RenderFrame(void)
{
  // This is were you render.
  // Keep in mind that this is not running in the engine thread!
  // Calling engine functions from here will cost numerous hours in the
  // debugger (or a stupid post on UC).

  RenderRectangle(0.375, 0.375, 0.25, 0.25, 0.1, 0.7, 0.3, 0.6);
}

/**
 * Hooked GLMContext::EndFrame
 */
void hkEndFrame(GLMContext *pThis)
{
  // Anything rendered in here will not show up on Steam screenshots

  SetupFrame();

  RenderFrame();

  s_oGLMContext_EndFrame(pThis);
}

/**
 * Installs the GLMContext::EndFrame hook
 * @return True on success, false otherwise
 */
bool InstallHook(void)
{
  /**
   * A handle to Valve's ToGL library
   */
  void *pToGL{ nullptr };

  // Wait for ToGL to be loaded
  // It won't be loaded right away if we're using LD_PRELOAD
  do
  {
    pToGL = dlopen("libtogl_client.so", RTLD_NOLOAD | RTLD_NOW);
    usleep(1000000);
  } while (pToGL == nullptr);

  // Lookup IDirect3DDevice::EndScene
  uintptr_t uiEndScene{ reinterpret_cast<uintptr_t>(dlsym(pToGL, "_ZN16IDirect3DDevice98EndSceneEv")) };

  // We don't need anything else from ToGL so we can close our handle
  dlclose(pToGL);

  if (uiEndScene == 0)
  {
    return false;
  }
  else
  {
    /**
     * The address of the call to GLMContext::EndFrame
     */
    uintptr_t uiCall{ uiEndScene + 0x0B };

    /**
     * Memory page size
     */
    size_t sPageSize{ static_cast<size_t>(sysconf(_SC_PAGE_SIZE)) };

    /**
     * The page the GLMContext::EndFrame call is in
     */
    void *pPage{ reinterpret_cast<void *>(uiCall - (uiCall % sPageSize)) };


    // Store the address of the original function
    s_oGLMContext_EndFrame = reinterpret_cast<fnGLMContext_EndFrame>(*reinterpret_cast<int *>(uiCall + 0x01) + uiCall + 0x05);

    // Allow write access on the page containing the call to GLMContext::EndFrame
    if (mprotect(pPage, sPageSize, PROT_READ | PROT_WRITE | PROT_EXEC) != 0)
    {
      return false;
    }
    else
    {
      // Override the call to GLMContext::EndFrame
      *reinterpret_cast<int *>(uiCall + 0x01) = static_cast<int>((reinterpret_cast<uintptr_t>(hkEndFrame) - uiCall) - 0x05);

      // Restore page protection, this isn't necessary
      if (mprotect(pPage, sPageSize, PROT_READ | PROT_EXEC) != 0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
  }
}

__attribute__((constructor))
void SOMain(void)
{
  std::thread thrMain{ InstallHook };

  thrMain.detach();
}
